﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MYARCH.Entities
{
    class BookOfTheWeek : Base
    {
        public string book_id { get; set; }
        public string comment { get; set; }
        public string user_id { get; set; }

    }
}
