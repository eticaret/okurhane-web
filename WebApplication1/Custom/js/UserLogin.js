﻿firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        document.getElementById('user_div').style.display = "none";
        document.getElementById('login_div').style.display = "block";
    }

    var user = firebase.auth().currentUser;

    if (user = ! null) {
        var email_id = user.email;
        document.getElementById('user_para').innerHTML = "Welcome User:" + email_id;

    }
    else {
        document.getElementById('user_div').style.display = "block";
        document.getElementById('login_div').style.display = "none";
    }
});


function userLogin() {
    var userEmail = document.getElementById('email_field').value;
    var userPass = document.getElementById('password_field').value;

    firebase.auth().signInWithEmailAndPassword(userEmail, userPass).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
    });

    window.alert("Error: " + errorMessage);
}

function resetPassword() {
    var email = document.getElementById('email').value;
    firebase.auth().sendPasswordResetEmail(email).then(function () {
        alert('Password Reset Email Sent!');
    }).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        if (errorCode === 'auth/user-not-found') {
            alert(errorMessage);
        }
        console.log(error);
    });
}
function logout() {
    firebase.auth().signOut();
}


