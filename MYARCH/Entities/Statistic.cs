﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MYARCH.Entities
{
    class Statistic : Base
    {
        public string user_id { get; set; }
        public string user_page_number { get; set; }
        public string like_count { get; set; }
        public string read_date { get; set; }
        
    }
}
