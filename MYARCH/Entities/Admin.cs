﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MYARCH.Entities
{
    class Admin : Base
    {
        public string admin_name { get; set; }
        public string email { get; set; }
        public string full_name { get; set; }
        public string password { get; set; }

    }
}
