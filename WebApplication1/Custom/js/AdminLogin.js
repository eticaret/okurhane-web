﻿
firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        
        document.getElementById("admin_div").style.display = "none"
        document.getElementById("login_div").style.display = "block"

        var user = firebase.auth().currrentUser;

        if (user != null){
            var email_id = user.email;
            document.getElementById("admin_para").innerHTML = "Welcome Admin : " + email_id;
        }

    } else {

        document.getElementById("admin_div").style.display = "block"
        document.getElementById("login_div").style.display = "none"
    }
});

function resetPassword() {
    var email = document.getElementById('email').value;
    firebase.auth().sendPasswordResetEmail(email).then(function () {
        alert('Password Reset Email Sent!');
    }).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        if (errorCode == 'auth/user-not-found') {
            alert(errorMessage);
        }
        console.log(error);
    });
}

function adminLogin() {

    var adminEmail = document.getElementById("email_field").value
    var adminPass = document.getElementById("password_field").value

    firebase.auth().signInWithEmailAndPassword(adminEmail, adminPass).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;
    });

    window.alert("Error : " + errorMessage);
};


function adminLogout() {
    firebase.auth().signOut();

}

