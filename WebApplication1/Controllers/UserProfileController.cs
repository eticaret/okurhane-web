﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class UserProfileController : Controller
    {
        // GET: UserProfile
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UserProfile()
        {
            return View();
        }

        public ActionResult MyLibrary()
        {
            return View();
        }

        public ActionResult Statistic()
        {
            return View();
        }

        public ActionResult ProfileSettings()
        {
            return View();
        }

        public ActionResult MyLikes()
        {
            return View();
        }

       public ActionResult MyCitations()
        {
            return View();
        }
    }
}