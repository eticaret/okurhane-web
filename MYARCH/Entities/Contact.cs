﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MYARCH.Entities
{
    class Contact : Base
    {
        public string quest_mail { get; set; }
        public string guest_message { get; set; }
        public string guest_name { get; set; }
    }
}
