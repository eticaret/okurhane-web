﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MYARCH.Entities
{
    class Feedbacks : Base
    {
        public string user_id { get; set; }
        public string feedback { get; set; }
    }
}
